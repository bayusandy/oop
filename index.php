<?php
require_once('animal.php');

$sheep = new Animal("shaun", 4, "no");
echo $sheep->get_name(); // "shaun"
echo $sheep->get_legs(); // 4
echo $sheep->get_cold_blooded(); // "no"


